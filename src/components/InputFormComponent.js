'use strict';

import React from 'react';
import config from 'config';
import PickerComponent from './PickerComponent';

class InputFormComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      ammount: 1,
      from: config.currencies[0],
      to: config.currencies[1]
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state);
  }

  handleInput = (event) => {
    const {id, value} = event.target;
    this.setState({
      [id]: value
    });
  }

  render() {
    const {ammount, from, to} = this.state;
    return (
      <form className='form-inline inputform-component'
        onSubmit={this.handleSubmit}
      >
        <div className='form-group mr-4'>
          <label htmlFor='ammount'>Ammount</label>
          <input id='ammount' type='number' className='form-control ml-2'
            value={ammount}
            onChange={this.handleInput}
          />
        </div>
        <div className='form-group mr-4'>
          <label htmlFor='from'>From</label>
          <PickerComponent
            id='from'
            value={from}
            onChange={this.handleInput}
          />
        </div>
        <div className='form-group mr-4'>
          <label htmlFor='to'>To</label>
          <PickerComponent
            id='to'
            value={to}
            onChange={this.handleInput}
          />
        </div>
        <button type='submit' className='btn btn-primary'>CONVERT!</button>
      </form>
    );
  }
}

InputFormComponent.displayName = 'InputFormComponent';

// Uncomment properties you need
InputFormComponent.propTypes = {
  onChange: React.PropTypes.func
};
// InputFormComponent.defaultProps = {};

export default InputFormComponent;
