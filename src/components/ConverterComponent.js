'use strict';

import React from 'react';
import InputFormComponent from './InputFormComponent';
import 'whatwg-fetch';

class ConverterComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      result: 0
    };
  }

  handleConvertion = (conversion) => {
    this.setState({
      result: 'Loading...'
    }, () => this.doConversion(conversion));
  }

  doConversion = ({ammount, from, to}) => {
    fetch(`http://api.fixer.io/latest?base=${from}&symbols=${to}`)
      .then((response) => response.json())
      .then((responseBody) => {
        const rate = responseBody.rates[to];
        const newResult = rate * ammount;
        this.setState({
          result: newResult
        });
      }).catch(() => {
        this.setState({
          result: 'Cenversion failed. Try again.'
        })
      });
  }

  render() {
    const {result} = this.state;
    return (
      <div className='converter-component'>
         <h5>Conversion details</h5>
         <InputFormComponent onSubmit={this.handleConvertion}/>
         <h5>Conversion result</h5>
         <p>{result}</p>
      </div>
    );
  }
}

ConverterComponent.displayName = 'ConverterComponent';

// Uncomment properties you need
// ConverterComponent.propTypes = {};
// ConverterComponent.defaultProps = {};

export default ConverterComponent;
